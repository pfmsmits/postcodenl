# OFBiz PostCode NL component #

Complement and validate Dutch addresses at Checkout with the free postcode Magento plugin from Postcode.nl.

### Data ###
This plugin uses the Postcode.nl API to validate en enrich Dutch address data. You can obtain a free account for the Postcode.nl API here.

### Licence ###
The code is available under the <@@> License. (see the LICENSE file)

### How do I get set up? ###

* Copy-paste into the Hot-Deploy folder of your OFBiz instance
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact